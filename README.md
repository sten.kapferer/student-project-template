Onboarding
==========

Afin de bien débuter votre projet, voici les différentes étapes à réaliser. Cochez-les au fur et à mesure. Lorsque toutes les étapes seront cochées, vous serez prêts !


Premier jour
------------

- [x] Télécharger ce template, créer un nouveau dépôt Git pour votre projet (p.ex. "tb-super-website"), et pusher le tout sur Gitlab (info: si vous avez un compte "externe", vous n'avez pas les permissions nécessaires pour créer un dépôt, dans ce cas c'est votre superviseur qui le fera à votre place).
- [x] Editer ce README et supprimer la première partie (cocher ces deux premières étapes en mettant un "x" entre les crochets, comme ça: [x])
- [x] Faire une séance d'introduction avec votre superviseur
- [x] Remplir les méta-données du projet ci-dessous (Voir [Nom du projet](#nom-du-projet))
- [x] Donner les accès à mon dépôt Gitlab à mon/mes superviseur/s (dans le panneau à gauche `Settings/Members`)


**Ressources :** Si vous n'êtes pas à l'aise avec Git, Docker ou d'autres outils, des tutoriels se trouvent sur le dépôt [jacky.casas/basic-tutorials](https://gitlab.forge.hefr.ch/jacky.casas/basic-tutorials), jettez-y un oeil.


Première semaine
----------------

- [x] Installer les logiciels requis sur votre ordinateur
- [x] Prendre en main les différentes technologies liées au projet
- [x] Rédiger le **cahier des charges** du projet (template disponible [ici](/docs/templates/CahierDesCharges-Template.docx))
- [x] Prévoir une séance hebdomadaire avec votre superviseur. Après chaque séance, vous devrez **rédiger un PV** et le mettre dans le dépôt du projet `/docs/PVs/`. Un [template LaTeX](/docs/PVs/template/pv.tex) et un [template Word](/docs/PVs/template/PV-Template.docx) se trouvent dans le même dossier)
- [ ] Mettre son code dans le dossier `code/` et renseigner dans le fichier `code/README.md` la façon d'installer et de lancer votre code (tout doit y figurer pour qu'une personne lambda puisse installer votre logiciel depuis zéro)

Une séance de présentation du cahier des charges sera organisée aux environs de la 2e semaine par votre superviseur (encore une fois, un [template](/docs/templates/Presentation-Template.pptx) existe).

Une présentation finale sera également organisée en temps voulu.

Voilà, vous êtes "onboardés" ! :)

--------------------------------------------------------------------------
Offboarding
===========

Voici une check-list pour être sûr d'avoir tout déposé sur Gitlab avant la fin de votre projet. Si tout est coché, ça devrait être ok.

- [ ] Tout le code se trouve dans le dossier `code/`
- [ ] Le fichier `code/README.md` contient toutes les explications nécessaire pour l'installation et le lancement de mon code
- [ ] Les PVs de toutes les séances se trouvent dans le dossier `docs/PVs/`
- [ ] Le cachier des charges se trouve dans le dossier `docs/`
- [ ] Les slides de la présentation du cahier des charges se trouve dans le dossier `docs/`
- [ ] Le rapport final se trouve dans le dossier `docs/`
- [ ] Les slides de la présentation finale du projet se trouvent dans le dossier `docs/`
- [ ] Une vidéo de démonstration de votre projet a été montée, envoyée à votre superviseur, et uploadée sur la [chaine Youtube de l'institut HumanTech](https://www.youtube.com/user/MISGchannel)
- [ ] J'ai complété la [fiche d'évaluation](docs/supervision-evaluation.md) de mon superviseur afin de l'aider à s'améliorer
- [ ] J'ai organisé un apéro de départ (optionnel, dépend de votre superviseur) ;)


--------------------------------------------------------------------------
Agent Conversationnel Vocal en 3D Web application
=============

Infos générales
---------------

- **Etudiant/stagiaire** : Sten Kapferer - Sten.Kapferer@edu.hefr.ch
- **Superviseur** : [Mira El Kamali](https://gitlab.forge.hefr.ch/mira.elkamali) - mira.elkamali@hefr.ch
- **Professeur** : [Elena Mugellini](https://gitlab.forge.hefr.ch/elena.mugellini) - elena.mugellini@hefr.ch
- **Professeur** : [Omar Abou Khaled](https://gitlab.forge.hefr.ch/omar.aboukhaled) - omar.aboukhaled@hefr.ch
- **Professeur** : [Leonardo Angelini](https://gitlab.forge.hefr.ch/leonardo.angelini) - leonardo.angelini@hefr.ch
- **Dates** : du 24.02.2021 au 26.05.2021


Contexte
--------

Ce projet de "Agent Conversationnel Vocal en 3D Web application" a été développé avec l'institut Humantech dans le cadre d'un projet européen ...


Description
-----------

Le but de ce projet est de développer une application web pour le coatch Nestore.


Contenu
-------

Ce dépôt contient toute la documentation relative au projet dans le dossier `docs/`. Le code du projet est dans le dossier `code/`.

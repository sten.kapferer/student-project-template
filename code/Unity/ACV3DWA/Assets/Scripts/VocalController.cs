using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VocalController : MonoBehaviour
{
    public GoogleCloudStreamingSpeechToText.StreamingRecognizer ScriptLaunchVocal;
    public RotateNestore RotateScript;

    private bool isEnable = true;

    public void EnabledVocal(){
        
        if(isEnable){
            ScriptLaunchVocal.gameObject.SetActive(true);
            ScriptLaunchVocal.StartListening();
        }
        else{
            ScriptLaunchVocal.StopListening();
            ScriptLaunchVocal.gameObject.SetActive(false);
        }
        isEnable = !isEnable;
       
    }

    public void OnDropDownValueChanged(Dropdown change){
        isEnable = false;
        ScriptLaunchVocal.OnLanguageChanged(change.value);
        EnabledVocal();
        EnabledVocal();
        RotateScript.rotateNestore(true);
    }
}

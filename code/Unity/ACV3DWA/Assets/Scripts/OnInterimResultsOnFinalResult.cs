using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class OnInterimResultsOnFinalResult : MonoBehaviour
{
    private TextMeshProUGUI completedText;

    public void OnInterimResults(string youSaid)
    {
        completedText.text = "Listening, you said: " + youSaid;
    }

    public void OnFinalResult(string youSaid)
    {
        completedText.text = "Done, I think you said: " + youSaid + "\n\nPress submit button if that's right";
    }
}

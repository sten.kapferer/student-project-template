using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeColor : MonoBehaviour
{
    [SerializeField]
    private GameObject nestore;

    private Renderer nestoreRenderer;
    private Color newNestroreColor;
    private Color InitialColor = new Color(1f, 1f, 1f);
    private float randomChannelOne, randomChannelTwo, randomChannelThree;
    private bool ActiveState = true;

    // Start is called before the first frame update
    void Start()
    {
        nestoreRenderer = nestore.GetComponent<Renderer>();
        gameObject.GetComponent<Button>().onClick.AddListener(ChangeNestoreColor);
    }

    private void ChangeNestoreColor()
    {
        if(ActiveState)
        {
            randomChannelOne = Random.Range(0f, 1f);
            randomChannelTwo = Random.Range(0f, 1f);
            randomChannelThree = Random.Range(0f, 1f);
            newNestroreColor = new Color(randomChannelOne, randomChannelTwo, randomChannelThree);
        }
        else
        {
            newNestroreColor = InitialColor;
        }

        ActiveState = !ActiveState;
        nestoreRenderer.material.SetColor("_Color", newNestroreColor);
    }
}

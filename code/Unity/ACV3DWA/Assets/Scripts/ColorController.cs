using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorController : MonoBehaviour
{
    public Renderer nestoreLightRenderer; // Renderer of Nestore "base_lumiere" mesh
    public GameObject lights; // GameObject containing all 18 lights of Nestore

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // Called before each color changing to avoid problems
    public void ColorReset()
    {
        //Reset lights
        string lightName = "";
        int i = 0;
        for (i = 0; i < 18; i++)
        {
            lightName = "light" + i;
            lights.transform.Find(lightName).gameObject.SetActive(false);
        }

        // Hide lights
        lights.SetActive(false);

        // Stop coroutines
        StopCoroutine("Circling");
        StopCoroutine("Blinking");
    }

    // Used when Nestore is in sleep mode
    public void NestoreColorBase()
    {
        ColorReset();
        
        nestoreLightRenderer.material.SetColor("_Color", new Color(1f, 1f, 1f, 0.86f)); // White
    }

    // Used when Nestore is listening to the user's voice
    public void NestoreColorListening()
    {
        ColorReset();
        // Debug.Log("Color Listening called");
        nestoreLightRenderer.material.SetColor("_Color", new Color(1f, 0.5f, 0f, 0.86f)); // Orange
    }

    // Used when Nestore is waiting from the answer from the API
    public void NestoreColorComputing()
    {
        ColorReset();

        lights.SetActive(true);
        nestoreLightRenderer.material.SetColor("_Color", new Color(1f, 1f, 1f, 0.86f));  // White
        StartCoroutine("Circling");
    }

    // Used when Nestore is speaking to the user
    public void NestoreColorSpeeking()
    {
        ColorReset();

        nestoreLightRenderer.material.SetColor("_Color", new Color(1f, 1f, 0f, 0.86f)); // Yellow
        StartCoroutine("Blinking");
    }

    IEnumerator Circling() 
    {
        int position = 0;
        while(true)
        {
            // Display 3 lights at position
            var lightName = "light" + position;
            lights.transform.Find(lightName).gameObject.SetActive(true);
            var lightNameNext = "light" + ((position+1)%18);
            lights.transform.Find(lightNameNext).gameObject.SetActive(true);
            lightNameNext = "light" + ((position+2)%18);
            lights.transform.Find(lightNameNext).gameObject.SetActive(true);

            position++;
            
            if(position > 17)
            {
                position = 0;
            }

            yield return new WaitForSeconds(.1f);

            lights.transform.Find(lightName).gameObject.SetActive(false); // Remove last light
        }
    }

    IEnumerator Blinking() 
    {
        bool isOn = false;
        while(true)
        {
            
            if(isOn)
            {
                nestoreLightRenderer.material.SetColor("_Color", new Color(1f, 1f, 0f, 0.86f)); // Yellow
            }
            else
            {
                nestoreLightRenderer.material.SetColor("_Color", new Color(1f, 1f, 1f, 0.86f)); // White
            }

            isOn = !isOn;
            yield return new WaitForSeconds(.8f);
        }
    }
}

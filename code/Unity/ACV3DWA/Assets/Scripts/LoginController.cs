using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoginController : MonoBehaviour
{
    public GameObject loginMenu;

    private SpeechController speechController;
    private string currentLang = "fr";

    [SerializeField]
    Text uiText;

    // Start is called before the first frame update
    void Start()
    {
        speechController = gameObject.GetComponent<SpeechController>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // Called by dropdown menu on value changed
    public void HandleLanguageDropdown(int val)
    {
        switch (val)
        {
        case 0:
            currentLang = "fr"; // Français
            break;
        case 1:
            currentLang = "en-US"; // English
            break;
        case 2:
            currentLang = "de"; // Deutsch
            break;
        default:
            currentLang = "en-US";
            break;
        }
    }

    public string getLanguage()
    {
        return currentLang;
    }

    public void ConfirmLogin()
    {
        loginMenu.SetActive(false);
        speechController.InitNestore(currentLang, uiText.text);
    }
}

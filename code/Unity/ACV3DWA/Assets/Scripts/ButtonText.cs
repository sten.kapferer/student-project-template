using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonText : MonoBehaviour
{
    public Text Textfield;

    public bool ActiveState = false;

    private ColorController colorController;

    void Start()
    {
        colorController = GameObject.FindWithTag("Controllers").GetComponent<ColorController>();
    }


    public void SetText()
    {
        if (ActiveState)
        {
            Textfield.text = "Start";
            colorController.NestoreColorBase();
        }
        else
        {
            Textfield.text = "Stop";
            colorController.NestoreColorListening();
        }
            

        ActiveState = !ActiveState;
    }
}

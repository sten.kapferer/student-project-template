using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateNestore : MonoBehaviour
{
    public Vector3 RotationEulerNestore = new Vector3(0, -45, -90);
    public Transform TransformInitial;
    private float diff = 30;
    bool rotateStatus = false;

    public void Start(){
        TransformInitial = transform;
        rotateStatus = true;
        rotateNestore();
    }

    public void rotateNestore(bool isRotate){
        if(rotateStatus){
            if(!isRotate)
                rotateNestore();
        }else{
            if(isRotate)
                rotateNestore();
        }
        
    }
    public void rotateNestore()
    {
        if(!rotateStatus)
        {
            this.transform.rotation = Quaternion.Euler(RotationEulerNestore);
            Vector3 newPos = transform.position;
            newPos.y += diff;
            transform.position = newPos;
        }
        else // debout
        {
            Vector3 newPos = transform.position;
            newPos.y -= diff;
            transform.position = newPos;
            transform.rotation = Quaternion.Euler(Vector3.zero);
            transform.position = TransformInitial.position;
        }
        rotateStatus = !rotateStatus;
    }
}
   

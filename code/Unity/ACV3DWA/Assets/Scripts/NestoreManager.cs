using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NestoreManager : MonoBehaviour
{
    public GameObject controllers;

    [SerializeField]
    Text uiText;

    private bool sleepMode;
    private SpeechController speechController;
    private ColorController colorController;
    private Vector3 RotationEulerNestore = new Vector3(0, -45, -90); // Rotation angles, can be changed / adapted

    // Start is called before the first frame update
    void Start()
    {
        sleepMode = true;
        speechController = controllers.GetComponent<SpeechController>();
        colorController = controllers.GetComponent<ColorController>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public bool getSleepMode()
    {
        return sleepMode;
    }

    public void changeNestoreMode()
    {
        // Debug.Log("ChangeNextoreMode called");
        if(sleepMode)
        {
            sleepMode = false;

            // Nestore is listening
            speechController.StartListening();

            // Apply rotation
            this.transform.rotation = Quaternion.Euler(RotationEulerNestore);

            // Apply color
            colorController.NestoreColorListening();

            // Text displayed
            uiText.text = "Listening...";
        }
        else
        {
            Debug.Log("SleepMode = true");
            sleepMode = true;

            // Nestore stops listening and speaking
            speechController.StopListening();
            speechController.StopSpeaking(); // Make the assistant stop talking if he is in the middle of a sentence

            // Reset rotation
            transform.rotation = Quaternion.Euler(Vector3.zero);

            // Reset color
            colorController.NestoreColorBase();

            // Text displayed
            uiText.text = "Sleep mode";
        }
    }
}

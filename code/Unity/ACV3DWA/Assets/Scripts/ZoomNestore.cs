using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoomNestore : MonoBehaviour
{
    public GameObject Nestore;
    public float Scale = 0.01f;

    private bool _ZoomIn;
    private bool _ZoomOut;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(_ZoomIn)
            Nestore.transform.localScale += new Vector3(Scale, Scale, Scale);

        if(_ZoomOut)
            Nestore.transform.localScale -= new Vector3(Scale, Scale, Scale);
    }

    public void OnPointerDownZoomOut()
    {
        _ZoomOut = true;
    }

    public void OnPointerUpZoomOut()
    {
        _ZoomOut = false;
    }

    public void OnPointerDownZoomIn()
    {
        _ZoomIn = true;
    }

    public void OnPointerUpZoomIn()
    {
        _ZoomIn = false;
    }
}

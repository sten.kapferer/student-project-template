using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Android;
using TextSpeech;
using UnityEngine.Networking;

// Source: https://www.youtube.com/watch?v=XRXbVtr1fog

public class SpeechController : MonoBehaviour
{
    public NestoreManager nestoreManager;

    const string LANG_CODE = "en-US"; // Not used anymore
    private ColorController colorController;
    private bool nestoreComputing; // True when we are waiting on the API. Used to avoid overlapping/overriding actions
    private string finalSpeechResult; // String of text we get from SpeechToText. Will be sent to API
    private string user_id; // Id of the user we get from login menu. Will be sent to API
    private string language; // language of the user we get from login menu. Will be sent to API

    [SerializeField]
    Text uiText;

    void Start()
    {
        // Not used, InitNestore acts as a Start() but is called after language and user id are registered
    }

    public void InitNestore(string currentLang, string userId)
    {
    Setup(currentLang); // TODO: Test if we can change language settings later
        colorController = gameObject.GetComponent<ColorController>();
        nestoreComputing = false;
        finalSpeechResult = "";
        user_id = userId;
        language = currentLang;

        // Register callbacks
#if UNITY_ANDROID
        SpeechToText.instance.onPartialResultsCallback = OnPartialSpeechResult;
#endif
        SpeechToText.instance.onResultCallback = OnFinalSpeechResult;
        SpeechToText.instance.onEndOfSpeechCallback = OnFinishTalking;
        SpeechToText.instance.onBeginningOfSpeechCallback = OnStartTalking;

        TextToSpeech.instance.onStartCallBack = OnSpeakStart;
        TextToSpeech.instance.onDoneCallback = OnSpeakStop;

        CheckPermission(); // Popup that asks for microphone permission on android
    }

    void CheckPermission()
    {
#if UNITY_ANDROID
        if(!Permission.HasUserAuthorizedPermission(Permission.Microphone))
        {
            Permission.RequestUserPermission(Permission.Microphone);
        }
#endif
    }

    #region Text to Speech
    public void StartSpeaking(string message)
    {
        TextToSpeech.instance.StartSpeak(message);
    }

    public void StopSpeaking()
    {
        nestoreComputing = false;
        TextToSpeech.instance.StopSpeak();
    }

    void OnSpeakStart()
    {
        Debug.Log("Talking started...");
    }

    void OnSpeakStop()
    {
        Debug.Log("talking stopped...");
        // TextToSpeech API finished talking, start listening to the user again
        nestoreComputing = false;
        if (!nestoreManager.getSleepMode())
        {
            colorController.NestoreColorListening();
            uiText.text = "Listening...";
            StartListening();
        }
    }
    #endregion

    #region Speech to text

    // Start recording user voice
    public void StartListening()
    {
        SpeechToText.instance.StartRecording();
    }

    // Stop recording user voice
    public void StopListening()
    {
        SpeechToText.instance.StopRecording();
    }

    // Callback once the final speech result has been determined
    void OnFinalSpeechResult(string result)
    {
        // Debug.Log("On final speech result called");

        if (!nestoreManager.getSleepMode() && !nestoreComputing)
        {
            uiText.text = result; // Display what has been recorded
        }    

        finalSpeechResult = result;
    }

    // Callback, only works on android (except if we change the lib for iOS -> Ask Laurent Chassot)
    void OnPartialSpeechResult(string result)
    {
        // Debug.Log("On partial speech result called");

        if (!nestoreManager.getSleepMode() && !nestoreComputing)
        {
            uiText.text = result; // Display what has been recorded
        }
    }

    // Callback when the user starts talking
    void OnStartTalking()
    {
        // Not used
    }
    
    // Callback when the user stops talking
    void OnFinishTalking()
    {
        StartCoroutine(FinishTalkingWait(2f));
    }
    #endregion

    IEnumerator FinishTalkingWait(float sec) 
    {
        yield return new WaitForSeconds(sec); // It's better to wait a little bit so the text effect looks better

        nestoreComputing = true;

        colorController.NestoreColorComputing();

        uiText.text = "Computing...";

        //StartCoroutine("DummyComputing"); // Used for simulation
        StartCoroutine("Upload");
    }

    // Simulate waiting for API results (unused now that we have Upload())
    IEnumerator DummyComputing() 
    {
        yield return new WaitForSeconds(4f);

        nestoreComputing = false;

        // API finished, start listening again
        if (!nestoreManager.getSleepMode())
        {
            colorController.NestoreColorListening();
            uiText.text = "Listening...";
            StartListening();
        }
    }

    IEnumerator Upload()
    {
        // Debug.Log("Begin upload");
        
        yield return new WaitForSeconds(2f); // TODO: Maybe remove later. used to simulate compute time in order to see the computing animation
   
        var www = new UnityWebRequest("http://160.98.47.126:3010/chatbot/tangible/message?user_id=Mira11&lang=en", "POST"); // TODO use variables user_id and language
        string jsonData = "{\"text\":\"hello\"}"; // TODO: replace "hello" with finalSpeechResult variable
        byte[] bodyRaw = System.Text.Encoding.UTF8.GetBytes(jsonData);
        www.uploadHandler = (UploadHandler) new UploadHandlerRaw(bodyRaw);
        www.downloadHandler = (DownloadHandler) new DownloadHandlerBuffer();
        www.SetRequestHeader("Content-Type", "application/json");

        www.timeout = 4; // Timeout after 4 secondes without answer
        yield return www.SendWebRequest();

        if (www.result != UnityWebRequest.Result.Success)
        {
            Debug.Log("WEB REQUEST FAILED: "+www.error);
        }
        else
        {
            // Debug.Log("Form upload complete!");

            // Parse JSON answer from Nestore API
            APIResponse deserializedObject = JsonUtility.FromJson<APIResponse>(www.downloadHandler.text);

            // Print Body
            Debug.Log(www.downloadHandler.text);

            if (!nestoreManager.getSleepMode())
            {
                colorController.NestoreColorSpeeking();
                uiText.text = deserializedObject.text; // www.downloadHandler.text;
                StartSpeaking(deserializedObject.text); // Text to speech
            }
        }

        // Debug.Log("End of Upload method");
    }

    // Use language code for setting
    void Setup(string code)
    {
        TextToSpeech.instance.Setting(code, 1, 1); // Language, pitch, rate
        SpeechToText.instance.Setting(code); // Language
    }
    
    // Used to parse JSON response we get from Nestore API
    [System.Serializable]
    public class APIResponse
    {
        public string text;
        public bool discussionEnd;
        public string[] intent;
    }
}
